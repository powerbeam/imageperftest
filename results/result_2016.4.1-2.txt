commit a27c155671e2addaafa607a462b783157d6b4ecb
Author: Tim Tan <tim.tan@powerbeaminc.com>
Date:   Sat Apr 1 06:27:29 2017 +0000

    Add median blur


#### Reading ####
file:   twoPeople.jpg
time:   1.695

#### Resizing ####
scale:  0.970
method: cubic
repeat: 1
total:  0.574
mean:   0.57359

#### Writing ####
file:   resized_scale=0.970_method=cubic_twoPeople.jpg
time:   2.404

#### Resizing ####
scale:  0.970
method: cubic
repeat: 5
total:  3.795
mean:   0.75900

#### Resizing ####
scale:  0.970
method: cubic
repeat: 20
total:  12.995
mean:   0.64973

#### Resizing ####
scale:  0.970
method: linear
repeat: 1
total:  0.296
mean:   0.29552

#### Writing ####
file:   resized_scale=0.970_method=linear_twoPeople.jpg
time:   2.430

#### Resizing ####
scale:  0.970
method: linear
repeat: 5
total:  2.208
mean:   0.44162

#### Resizing ####
scale:  0.970
method: linear
repeat: 20
total:  6.751
mean:   0.33757

#### Rotation ####
scale:  0.970
angle:  2.000
centerX: 0.000
centerY: 0.000
method: cubic
repeat: 1
total:  1.063
mean:   1.06294

#### Writing ####
file:   rotated_scale=0.970_angle=2.000_centerX=0.000_centerY=0.000_method=cubic_twoPeople.jpg
time:   2.569

#### Rotation ####
scale:  0.970
angle:  2.000
centerX: 0.000
centerY: 0.000
method: cubic
repeat: 5
total:  5.462
mean:   1.09245

#### Rotation ####
scale:  0.970
angle:  2.000
centerX: 0.000
centerY: 0.000
method: cubic
repeat: 20
total:  22.122
mean:   1.10611

#### Rotation ####
scale:  0.970
angle:  2.000
centerX: 0.000
centerY: 0.000
method: linear
repeat: 1
total:  0.570
mean:   0.57013

#### Writing ####
file:   rotated_scale=0.970_angle=2.000_centerX=0.000_centerY=0.000_method=linear_twoPeople.jpg
time:   2.464

#### Rotation ####
scale:  0.970
angle:  2.000
centerX: 0.000
centerY: 0.000
method: linear
repeat: 5
total:  2.872
mean:   0.57439

#### Rotation ####
scale:  0.970
angle:  2.000
centerX: 0.000
centerY: 0.000
method: linear
repeat: 20
total:  11.509
mean:   0.57547

#### Median Blur ####
kSize:  3
repeat: 1
total:  0.190
mean:   0.18954

#### Writing ####
file:   medianBlurred_kSize=3_twoPeople.jpg
time:   2.483

#### Median Blur ####
kSize:  3
repeat: 5
total:  0.950
mean:   0.19003

#### Median Blur ####
kSize:  3
repeat: 20
total:  3.806
mean:   0.19029

