#include <cv.h>
#include <highgui.h>
#include <opencv2/core/core.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>
#include <time.h>

using namespace cv;
using namespace cv::gpu;
using namespace std;

class Timer {
public:
    struct timespec start;
    struct timespec stop;
    double elapsed;

    Timer()
        : elapsed(0)
    {
    }

    void tic()
    {
        clock_gettime(CLOCK_REALTIME, &start);
    }

    void toc()
    {
        clock_gettime(CLOCK_REALTIME, &stop);
        double sec = stop.tv_sec - start.tv_sec;
        double nsec = (stop.tv_nsec - start.tv_nsec) / 1e9;
        elapsed = sec + nsec;
    }

    void print()
    {
        printf("time:   %-.3f\n", elapsed);
    }

    void print(int repeat)
    {
        printf("repeat: %d\n", repeat);
        printf("total:  %-.3f\n", elapsed);
        printf("mean:   %-.5f\n", elapsed / repeat);
    }
};

class TestParam {
public:
    double scale;
    double angle;
    double centerX;
    double centerY;
    int method;
    int kSize;

    TestParam()
        : scale(0)
        , angle(361)
        , centerX(-1)
        , centerY(-1)
        , method(-1)
        , kSize(-1)
    {
    }

    void print()
    {
        if (scale > 0) {
            printf("scale:  %-.3f\n", scale);
        }
        if (angle < 360) {
            printf("angle:  %-.3f\n", angle);
        }
        if (centerX >= 0) {
            printf("centerX: %-.3f\n", centerX);
        }
        if (centerY >= 0) {
            printf("centerY: %-.3f\n", centerY);
        }
        if (method == INTER_LINEAR) {
            printf("method: linear\n");
        } else if (method == INTER_CUBIC) {
            printf("method: cubic\n");
        }
        if (kSize > 0) {
            printf("kSize:  %d\n", kSize);
        }
    }

    string str()
    {
        char line[1024] = { 0 };
        int i = 0;
        if (scale > 0) {
            i += sprintf(line + i, "scale=%-.3f_", scale);
        }
        if (angle < 360) {
            i += sprintf(line + i, "angle=%-.3f_", angle);
        }
        if (centerX >= 0) {
            i += sprintf(line + i, "centerX=%-.3f_", centerX);
        }
        if (centerY >= 0) {
            i += sprintf(line + i, "centerY=%-.3f_", centerY);
        }
        if (method == INTER_LINEAR) {
            i += sprintf(line + i, "method=linear_");
        } else if (method == INTER_CUBIC) {
            i += sprintf(line + i, "method=cubic_");
        }
        if (kSize > 0) {
            i += sprintf(line + i, "kSize=%d_", kSize);
        }
        return string(line);
    }
};

void writeImage(Mat outImage, string outName)
{
    printf("\n#### Writing ####\n");
    Timer t;
    t.tic();
    imwrite(outName, outImage);
    printf("file:   %s\n", outName.c_str());
    t.toc();
    t.print();
}

GpuMat copyToGpu(Mat cpuMat)
{
    printf("\n#### Copy CPU -> GPU ####\n");
    Timer t;
    t.tic();
    GpuMat gpuMat(cpuMat);
    t.toc();
    t.print();
    return gpuMat;
}

Mat copyToCpu(GpuMat gpuMat)
{
    printf("\n#### Copy GPU -> CPU ####\n");
    Timer t;
    t.tic();
    Mat cpuMat(gpuMat);
    t.toc();
    t.print();
    return cpuMat;
}

void testResize(Mat image, TestParam p, int repeat, string imageName, bool write)
{
    printf("\n#### Resizing - CPU ####\n");
    Mat outImage;
    p.print();

    // run once first before timing
    resize(image, outImage, Size(), p.scale, p.scale, p.method);

    Timer t;
    t.tic();
    for (int i = 0; i < repeat; i++) {
        resize(image, outImage, Size(), p.scale, p.scale, p.method);
    }
    t.toc();
    t.print(repeat);

    if (write) {
        writeImage(outImage, "resized_" + p.str() + imageName);
    }
}

void testResizeGpu(Mat image, TestParam p, int repeat, string imageName, bool write)
{
    GpuMat gpuIn = copyToGpu(image);

    printf("\n#### Resizing - GPU ####\n");
    p.print();

    // run once first before timing
    GpuMat gpuOut;
    gpu::resize(gpuIn, gpuOut, Size(), p.scale, p.scale, p.method);

    Timer t;
    t.tic();
    for (int i = 0; i < repeat; i++) {
        gpu::resize(gpuIn, gpuOut, Size(), p.scale, p.scale, p.method);
    }
    t.toc();
    t.print(repeat);

    if (write) {
        Mat outImage = copyToCpu(gpuOut);
        writeImage(outImage, "resizedGpu_" + p.str() + imageName);
    }
}

void testRotate(Mat image, TestParam p, int repeat, string imageName, bool write)
{
    printf("\n#### Rotation - CPU ####\n");
    Mat outImage;
    p.print();

    Point2f center(p.centerX, p.centerY);
    Mat rotator = getRotationMatrix2D(center, p.angle, p.scale);

    // run once first before timing
    warpAffine(image, outImage, rotator, Size(), p.method);

    Timer t;
    t.tic();
    for (int i = 0; i < repeat; i++) {
        warpAffine(image, outImage, rotator, Size(), p.method);
    }
    t.toc();
    t.print(repeat);

    if (write) {
        writeImage(outImage, "rotated_" + p.str() + imageName);
    }
}

void testRotateGpu(Mat image, TestParam p, int repeat, string imageName, bool write)
{
    GpuMat gpuIn = copyToGpu(image);

    printf("\n#### Rotation - GPU ####\n");
    p.print();

    Point2f center(p.centerX, p.centerY);
    Mat rotator = getRotationMatrix2D(center, p.angle, p.scale);

    // run once first before timing
    GpuMat gpuOut = gpuIn.clone();
    gpu::warpAffine(gpuIn, gpuOut, rotator, gpuOut.size(), p.method);

    Timer t;
    t.tic();
    for (int i = 0; i < repeat; i++) {
        gpu::warpAffine(gpuIn, gpuOut, rotator, gpuOut.size(), p.method);
    }
    t.toc();
    t.print(repeat);

    if (write) {
        Mat outImage = copyToCpu(gpuOut);
        writeImage(outImage, "rotatedGpu_" + p.str() + imageName);
    }
}

void testMedianBlur(Mat image, TestParam p, int repeat, string imageName, bool write)
{
    printf("\n#### Median Blur - CPU ####\n");
    Mat outImage;
    p.print();

    // run once first before timing
    medianBlur(image, outImage, p.kSize);

    Timer t;
    t.tic();
    for (int i = 0; i < repeat; i++) {
        medianBlur(image, outImage, p.kSize);
    }
    t.toc();
    t.print(repeat);

    if (write) {
        writeImage(outImage, "medianBlurred_" + p.str() + imageName);
    }
}

// medianFilter function is not available in gpu::
#if 1
void testMedianBlurGpu(Mat image, TestParam p, int repeat, string imageName, bool write)
{
}
#else
void testMedianBlurGpu(Mat image, TestParam p, int repeat, string imageName, bool write)
{
    GpuMat gpuIn = copyToGpu(image);

    printf("\n#### Median Blur - GPU ####\n");
    p.print();

    // run once first before timing
    GpuMat gpuOut;
    gpu::medianBlur(gpuIn, gpuOut, p.kSize);

    Timer t;
    t.tic();
    for (int i = 0; i < repeat; i++) {
        gpu::medianBlur(gpuIn, gpuOut, p.kSize);
    }
    t.toc();
    t.print(repeat);

    if (write) {
        Mat outImage = copyToCpu(gpuOut);
        writeImage(outImage, "medianBlurredGpu_" + p.str() + imageName);
    }
}
#endif

int main(int argc, char** argv)
{
    if (argc != 2) {
        printf("Specify image file\n");
        return -1;
    }

    // read image
    Timer t;
    t.tic();
    printf("\n#### Reading ####\n");
    string imageName(argv[1]);
    printf("file:   %s\n", imageName.c_str());
    Mat image = imread(imageName, 1);
    if (!image.data) {
        printf("Failed to read image\n ");
        return -1;
    }
    t.toc();
    t.print();

    // GPU support
    printf("\n#### GPU Support ####\n");
    printf("nCuda:  %d\n", gpu::getCudaEnabledDeviceCount());

    // 1 - resizing test
    {
        TestParam p;
        p.scale = 0.97;
        p.method = INTER_CUBIC;
        testResize(image, p, 1, imageName, true);
        testResizeGpu(image, p, 1, imageName, true);
        testResize(image, p, 5, imageName, false);
        testResizeGpu(image, p, 5, imageName, false);
        testResize(image, p, 20, imageName, false);
        testResizeGpu(image, p, 20, imageName, false);

        p.method = INTER_LINEAR;
        testResize(image, p, 1, imageName, true);
        testResizeGpu(image, p, 1, imageName, true);
        testResize(image, p, 5, imageName, false);
        testResizeGpu(image, p, 5, imageName, false);
        testResize(image, p, 20, imageName, false);
        testResizeGpu(image, p, 20, imageName, false);
    }

    // 2 - rotating test
    {
        TestParam p;
        p.angle = 2.0;
        p.scale = 0.97;
        p.centerX = 0.0;
        p.centerY = 0.0;
        p.method = INTER_CUBIC;
        testRotate(image, p, 1, imageName, true);
        testRotateGpu(image, p, 1, imageName, true);
        testRotate(image, p, 5, imageName, false);
        testRotateGpu(image, p, 5, imageName, false);
        testRotate(image, p, 20, imageName, false);
        testRotateGpu(image, p, 20, imageName, false);

        p.method = INTER_LINEAR;
        testRotate(image, p, 1, imageName, true);
        testRotateGpu(image, p, 1, imageName, true);
        testRotate(image, p, 5, imageName, false);
        testRotateGpu(image, p, 5, imageName, false);
        testRotate(image, p, 20, imageName, false);
        testRotateGpu(image, p, 20, imageName, false);
    }

    // 3 - median blur test
    {
        TestParam p;
        p.kSize = 3;
        testMedianBlur(image, p, 1, imageName, true);
        testMedianBlurGpu(image, p, 1, imageName, true);
        testMedianBlur(image, p, 5, imageName, false);
        testMedianBlurGpu(image, p, 5, imageName, false);
        testMedianBlur(image, p, 20, imageName, false);
        testMedianBlurGpu(image, p, 20, imageName, false);
    }

    return 0;
}
